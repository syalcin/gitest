package com.app.company.favorites;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by salih.yalcin on 7.12.2015.
 */
public class FavoritesActivity extends Activity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Map<String, String> mFavorites;
    ArrayList<String> mArr;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        preferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        editor = preferences.edit();
        lv= (ListView)findViewById(R.id.listViewFavorites);
        mFavorites = (Map<String, String>) preferences.getAll();
        mArr = new ArrayList<>(mFavorites.values());
        //..

        final ArrayAdapter<String> mAdapter = new ArrayAdapter<>(FavoritesActivity.this,android.R.layout.simple_list_item_1,mArr);
        lv.setAdapter(mAdapter);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String mSelectedValue = mArr.get(position);
                String mSelectedKey = getKeyByValue(mFavorites,mSelectedValue);
                mArr.remove(position);
                mAdapter.notifyDataSetChanged();
                editor.remove(mSelectedKey);
                editor.apply();

            }
        });

        editor.apply();

    }



    public static <K, V> K getKeyByValue(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}