package com.app.company.favorites;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by akife.babacan on 30.7.2015.
 */
public class SearchAdapter extends ArrayAdapter<String> {


    SparseBooleanArray mStates;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Map<String, String> mFavorites;
    private LayoutInflater vi;
    private ArrayList<String> originalItems; // Listview'de gösterilen değerler
    private ArrayList<String> filteredItems; // Filtrelenmiş değerler
    private ArrayList<String> notFilteredItems; //Başta kullandığımız filtrelenmemiş değerler.
    private Filter filter;


    public SearchAdapter(Activity context, ArrayList<String> arraylist, ArrayList<String> notfiltered) {
        super(context, R.layout.adapter_layout_search, arraylist);
        vi = context.getLayoutInflater();
        this.originalItems = arraylist;
        this.filteredItems = arraylist;
        this.notFilteredItems = notfiltered;
        this.filter = new ModelFilter();
        mStates = new SparseBooleanArray();
        preferences = PreferenceManager
                .getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        mFavorites = (Map<String, String>) preferences.getAll();
        editor.apply();

    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ModelFilter();
        }
        return filter;
    }


    public void updateReceiptsList() {

        mFavorites = (Map<String, String>) preferences.getAll();
        mStates.clear();
        this.notifyDataSetChanged();
    }

    public void masterMethod(){
        Log.d("Test","Test");
    }


    @Override
    public int getCount() {
        return originalItems.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

        final String getInfoUtilsList = filteredItems.get(position);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = vi.inflate(R.layout.adapter_layout_search, null);
            holder.buildName = (TextView) convertView.findViewById(R.id.countytext);
          //  holder.mImageView = (ImageView) convertView.findViewById(R.id.imageViewCard);
            holder.mFavorite = (ImageButton) convertView.findViewById(R.id.favorites);
            convertView.setTag(holder);
            holder.mFavorite.setTag(position);


        } else {

            holder = (ViewHolder) convertView.getTag();
            holder.mFavorite.getTag();


        }

        holder.buildName.setText(getInfoUtilsList);

        int pos = notFilteredItems.indexOf(getInfoUtilsList);

        if (!mStates.get(pos) && mFavorites.get(String.valueOf(pos)) == null) {
            holder.mFavorite.setBackgroundResource(R.drawable.notfavorited);
            mStates.put(pos, false);
        } else {
            holder.mFavorite.setBackgroundResource(R.drawable.favorited);
            mStates.put(pos, true);
        }


        holder.mFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = notFilteredItems.indexOf(getInfoUtilsList);


                if (!mStates.get(pos)) {
                    holder.mFavorite.setBackgroundResource(R.drawable.favorited);
                    mStates.put(pos, true);
                    editor.putString(String.valueOf(pos), getInfoUtilsList);

                } else {
                    holder.mFavorite.setBackgroundResource(R.drawable.notfavorited);
                    mStates.put(pos, false);
                    editor.remove(String.valueOf(pos));

                }
                editor.apply();
            }
        });


        return convertView;
    }

    @Override
    public String getItem(int i) {
        return originalItems.get(i);
    }

    @Override
    public boolean hasStableIds() {
        return android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;
    }

    @Override
    public long getItemId(int position) {
        return filteredItems.get(position).hashCode();
    }

    @Override
    public int getViewTypeCount() {

        return getCount();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    private class ModelFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            String prefix = constraint.toString().toLowerCase();

            if (prefix.length() == 0) {
                ArrayList<String> list = new ArrayList<>(notFilteredItems);
                results.values = list;
                results.count = list.size();
            } else {
                final ArrayList<String> list = new ArrayList<>(notFilteredItems);
                final ArrayList<String> nlist = new ArrayList<>();
                int count = list.size();

                for (int i = 0; i < count; i++) {
                    final String dataNames = list.get(i);
                    final String value = dataNames.toLowerCase();

                    if (value.contains(prefix)) {
                        nlist.add(dataNames);
                    }

                }
                results.values = nlist;
                results.count = nlist.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {


            if (results.values == null)
                return;
            filteredItems = (ArrayList<String>) results.values;
            originalItems.clear();
            notifyDataSetChanged();
            int count = filteredItems.size();
            //..
            for (int i = 0; i < count; i++) {
                originalItems.add(filteredItems.get(i));
                notifyDataSetInvalidated();
            }


        }

    }

    class ViewHolder {
        TextView buildName;
        ImageView mImageView;
        ImageButton mFavorite;
    }


}