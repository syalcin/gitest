package com.app.company.favorites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by akife.babacan on 20.7.2015.
 */

public class SearchActivity extends AppCompatActivity {
    public static ArrayList<String> sfOriginalList;
    public static ArrayList<String> sfNotFilteredList;
    public static final String TAG = "SYLCN";
    ListView listView;
    EditText editSearch;
    SearchAdapter mAdapter;
    String text;




    //Ben masterda bu değişiklikleri yapıyorum.

    //Porcheden selamlar.

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        sfOriginalList = new ArrayList<>();
        sfNotFilteredList = new ArrayList<>();
       /* for (int i = 0; i < 17; i++) {
            SearchUtils sUtils = new SearchUtils();
            sUtils.setmBuildName("IBB" + String.valueOf(i));
            sfNotFilteredList.add(sUtils);
            sfOriginalList.add(sUtils);
        }*/

        sfNotFilteredList.add("AVCILAR");
        sfNotFilteredList.add("ADALAR");
        sfNotFilteredList.add("BAKIRKÖY");
        sfNotFilteredList.add("BAHÇELİEVLER");
        sfNotFilteredList.add("BAŞAKŞEHİR");
        sfNotFilteredList.add("GÜNGÖREN");
        sfNotFilteredList.add("FATİH");
        sfNotFilteredList.add("GAZİOSMANPAŞA");
        sfNotFilteredList.add("ETİLER");
        sfNotFilteredList.add("SARIYER");
        sfNotFilteredList.add("SAMATYA");
        sfNotFilteredList.add("YENİBOSNA");
        sfNotFilteredList.add("ZEYTİNBURNU");
        sfNotFilteredList.add("ZEKERİYAKÖY");
        sfNotFilteredList.add("ÇORLU");


        sfOriginalList.add("AVCILAR");
        sfOriginalList.add("ADALAR");
        sfOriginalList.add("BAKIRKÖY");
        sfOriginalList.add("BAHÇELİEVLER");
        sfOriginalList.add("BAŞAKŞEHİR");
        sfOriginalList.add("GÜNGÖREN");
        sfOriginalList.add("FATİH");
        sfOriginalList.add("GAZİOSMANPAŞA");
        sfOriginalList.add("ETİLER");
        sfOriginalList.add("SARIYER");
        sfOriginalList.add("SAMATYA");
        sfOriginalList.add("YENİBOSNA");
        sfOriginalList.add("ZEYTİNBURNU");
        sfOriginalList.add("ZEKERİYAKÖY");
        sfOriginalList.add("ÇORLU");



        mAdapter = new SearchAdapter(this, sfOriginalList, sfNotFilteredList);

        listView = (ListView) findViewById(R.id.listView2);
        listView.setTextFilterEnabled(true);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick - sfOriginalList: " + sfOriginalList.get(position));
                Log.d(TAG, "onItemClick - sfNotFilteredList: " + sfNotFilteredList.get(position));
                Toast.makeText(SearchActivity.this,"onItemClick - sfOriginalList: " + sfOriginalList.get(position),Toast.LENGTH_SHORT).show();

            }
        });

        editSearch = (EditText) findViewById(R.id.editText);

        editSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }


            @Override
            public void afterTextChanged(Editable s) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                text = editSearch.getText().toString().toLowerCase(Locale.getDefault());
                SearchActivity.this.mAdapter.getFilter().filter(text);


            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_favoritesmenu) {
            Intent i = new Intent(SearchActivity.this, FavoritesActivity.class);
            startActivityForResult(i, 0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            mAdapter.updateReceiptsList();
        }
    }




}
/*
public class SearchActivity extends AppCompatActivity {

    public static ArrayList<String> sfOriginalList;
    public static ArrayList<java.lang.String> sfNotFilteredList;
    ListView listView;
    EditText editSearch;
    SearchAdapter mAdapter;
    java.lang.String text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        sfOriginalList = new ArrayList<>();
        sfNotFilteredList = new ArrayList<>();
      */
/*  for (int i = 0; i < 17; i++) {
            SearchUtils sUtils = new SearchUtils();
            sUtils.setmBuildName("IBB" + String.valueOf(i));

        }*//*

        sfNotFilteredList.add("AVCILAR");
        sfNotFilteredList.add("ADALAR");
        sfNotFilteredList.add("BAKIRKÖY");
        sfNotFilteredList.add("BAHÇELİEVLER");
        sfNotFilteredList.add("BAŞAKŞEHİR");
        sfNotFilteredList.add("GÜNGÖREN");
        sfNotFilteredList.add("FATİH");
        sfNotFilteredList.add("GAZİOSMANPAŞA");
        sfNotFilteredList.add("ETİLER");
        sfNotFilteredList.add("SARIYER");
        sfNotFilteredList.add("SAMATYA");
        sfNotFilteredList.add("YENİBOSNA");
        sfNotFilteredList.add("ZEYTİNBURNU");
        sfNotFilteredList.add("ZEKERİYAKÖY");
        sfNotFilteredList.add("ÇORLU");


        sfOriginalList.add("AVCILAR");
        sfOriginalList.add("ADALAR");
        sfOriginalList.add("BAKIRKÖY");
        sfOriginalList.add("BAHÇELİEVLER");
        sfOriginalList.add("BAŞAKŞEHİR");
        sfOriginalList.add("GÜNGÖREN");
        sfOriginalList.add("FATİH");
        sfOriginalList.add("GAZİOSMANPAŞA");
        sfOriginalList.add("ETİLER");
        sfOriginalList.add("SARIYER");
        sfOriginalList.add("SAMATYA");
        sfOriginalList.add("YENİBOSNA");
        sfOriginalList.add("ZEYTİNBURNU");
        sfOriginalList.add("ZEKERİYAKÖY");
        sfOriginalList.add("ÇORLU");

        mAdapter = new SearchAdapter(this, sfOriginalList, sfNotFilteredList);

        listView = (ListView) findViewById(R.id.listView2);
        listView.setTextFilterEnabled(true);
        listView.setAdapter(mAdapter);

        editSearch = (EditText) findViewById(R.id.editText);

        editSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }


            @Override
            public void afterTextChanged(Editable s) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                text = editSearch.getText().toString().toLowerCase(Locale.getDefault());
                SearchActivity.this.mAdapter.getFilter().filter(text);


            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_favoritesmenu) {
            Intent i = new Intent(SearchActivity.this, FavoritesActivity.class);
            startActivityForResult(i, 0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            mAdapter.updateReceiptsList();
        }
    }




}*/
